const getApiInventory = require('./GET@api_v1_inventory');
const postApiInventory = require('./POST@api_v1_inventory');
const readinessProbe = require('./readinessProbe');
const notFound_404 = require('./notFound_404');


const allowedMethods = ['GET', 'POST'];
const allowedPaths = ['/api/v1/inventory', '/readiness'];
const allowedGETpaths = ['/api/v1/inventory', '/readiness'];
const allowedPOSTpaths = ['/api/v1/inventory'];

function routes() {
  return (req, res) => {
    const { method, url } = req;

    console.group('Route Handler - Inventory Service');
    console.log({ method, url })
    console.groupEnd();

    if (!allowedMethods.includes(method) || !allowedPaths.includes(url) ||
        (method === 'GET' && !allowedGETpaths.includes(url)) ||
        (method === 'POST' && !allowedPOSTpaths.includes(url))
    ) notFound_404(req, res);

    if (method === 'GET' && url === '/api/v1/inventory') getApiInventory(req, res);

    if (method === 'POST' && url === '/api/v1/inventory') postApiInventory(req, res);

    if (url === '/readiness') readinessProbe(req, res);
  };
}


module.exports = routes;
