function getHome(req, res) {
  const state = global.state;
  const resBody = JSON.stringify(state);

  res.writeHead(200, {
    'Content-Type': 'application/json',
    'Content-Length': Buffer.byteLength(resBody)
  });
  res.write(resBody);
  res.end();
}


module.exports = getHome;
