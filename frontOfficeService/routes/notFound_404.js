const invalidRequest = (method, url) => `
  <html>
    <body>
      <h1>404: NOT FOUND</h1>
      <p>request: ${method} ${url}</p>
      <h2>Allowed Requests:</h2>
      <ul>
        <li>GET /</li>
        <li>GET /inventory-service</li>
        <li>GET /readiness</li>
        <li>
          POST / <br/>
          body: {}
        </li>
        <li>
          POST /inventory-service <br/>
          body: {}
        </li>
      </ul>
    </body>
  </html>
`;

function notFound_404(req, res) {
  const { method, url } = req;
  console.log('404', { method, url });
  const resBody = invalidRequest(method, url);

  res.writeHead(404, {
    'Content-Type': 'text/html',
    'Content-Length': Buffer.byteLength(resBody)
  });
  res.write(resBody);
  res.end();
}


module.exports = notFound_404;
