const http = require('http');


const options = {
  hostname: process.env.INVENTORY_SERVICE_HOST || 'localhost',
  port: process.env.INVENTORY_SERVICE_PORT || 3100,
  path: '/api/v1/inventory'
};

function getInventoryService(req, res) {
  const getRequest = http.get(options, resp => {
    let respBody = [];

    console.group('FOS - GET /Inventory-service Response');
    console.log(`STATUS: ${resp.statusCode}`);
    console.log(`HEADERS: ${JSON.stringify(resp.headers)}`);
    console.groupEnd();

    resp.on('data', chunk => respBody.push(chunk));
    resp.on('end', () => {
      respBody = Buffer.concat(respBody).toString();

      res.writeHead(200, {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(respBody)
      });
      res.write(respBody);
      res.end();
    });
  });

  getRequest.end();
}


module.exports = getInventoryService;
